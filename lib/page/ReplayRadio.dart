
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gu_radio/helper/api.dart';
import 'package:gu_radio/provider/GuStore.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:provider/provider.dart';

class ReplayRadio extends StatefulWidget {
  const ReplayRadio({Key key}) : super(key: key);

  @override
  _ReplayRadioState createState() => _ReplayRadioState();
}

class _ReplayRadioState extends State<ReplayRadio> {
  var data;
  bool isLoading = true;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  GuStore guStore;
  List dataList = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async{
      await getDataPostReplays();
    });
  }

  getDataPostReplays() async{
    data = await Network().fetchData("wp/v2/replay");
    if(!data.contains('noConnection')) {
      for(var item in data ){
       if(item['slug'] != "bandeaux-pubs" && item['slug'] != "coup-de-coeur"){
        if(dataList.isNotEmpty && dataList[0]['slug'] != "bandeaux-pubs" && dataList[0]['slug'] != "coup-de-coeur"){
           dataList.insert(0, item);
         }
         else dataList.add(item);
       }
       else if(item['slug'] == "bandeaux-pubs"){
         if(dataList.isNotEmpty && dataList.length > 3 ) {
           dataList[2] = item;
         }
         else dataList.add(item);
       }
       else if(item['slug'] == "coup-de-coeur"){
         if(dataList.isNotEmpty && (dataList.length > 1 || dataList.length == 1 ) ){
           if(dataList.length == 1 ){
             if(dataList[0]['slug'] == "bandeaux-pubs")
               dataList.insert(0, item);
             else dataList.add(item);
           } else dataList[1] = item;
         }
         else{
           if(dataList.length > 2 || dataList.length == 2)
             dataList[1] = item;
           else dataList.add(item);
         }
       }
       }
      setState(() {
        isLoading = false;
      });
      } else if(data['noConnection']) showSnackBar();
  }

  showSnackBar(){
    final snackbar = SnackBar(content: Text('Verifier votre connexion', style: TextStyle(color: Colors.white)), backgroundColor: Colors.black);
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

 List widgetReplays(data){
      List widgets = [];
      int compte = 0;
      for(int i = 0; i < dataList.length; i++ ){
        var item = dataList[i];
        if(item['slug'] != 'coup-de-coeur' && item["slug"] != "bandeaux-pubs"){
          List<Widget> kwidget = [];
          for(int k = 0; k< item['articles'].length; k++){
            var kItem = item['articles'][k];
            print('ktItem');
            print(kItem['audio']);
            kwidget.add(
                InkWell(
                  onTap: (){
                    Navigator.pushNamed(context, '/playerPodcast',
                        arguments: {
                          'audio': kItem['audio'],
                          'title': kItem['title'],
                          'audioTitle' : kItem['audio_title'],
                          'cover' : kItem['cover'],
                          'headerTitle' : item['name'].toUpperCase(),
                          'duration' : Duration(seconds: kItem['duree']),
                          'type': 'replay'
                        });
                  },
                  child: Container(
                    padding: EdgeInsets.only(left: 18.0),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 30 / 100,
                          height: MediaQuery.of(context).size.width * 30 / 100,
                          child: Center(
                              child: Image.network(kItem['cover'], fit: BoxFit.cover)
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.all(8.0),
                            width: MediaQuery.of(context).size.width * 30 / 100,
                            color: Colors.black,
                            child: Text(kItem['title'],
                              style: TextStyle(
                                  color: Colors.white),
                                  textAlign: TextAlign.start,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis
                              )
                        )
                      ],
                    ),
                  ),
                )
            );
          }
          List title = [];
          if(item['name'].contains("(")){
             title = item['name'].split('(');
          } else{
            title.insert(0, item['name']);
          }
         Widget viewRender =  Column(
           crossAxisAlignment: CrossAxisAlignment.start,
           children: [
             Padding(
               padding: const EdgeInsets.only(left: 15.0),
               child: new RichText(
                 text: new TextSpan(
                   style: new TextStyle(
                     fontSize: 14.0,
                     color: Colors.black,
                   ),
                   children: <TextSpan>[
                     new TextSpan(text: title[0].toUpperCase(), style: new TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 16.0)),
                     if(title.asMap().containsKey(1))
                        new TextSpan(text: '('+title[1], style: new TextStyle(fontWeight: FontWeight.bold, color: Colors.yellow, fontSize: 16.0)),
                   ],
                 ),
               ),
             ),
             SizedBox(height: 10.0,),
             SingleChildScrollView(
               scrollDirection: Axis.horizontal,
               child: Row(
                 children: kwidget,
               ),
             ),
           ],
         );
          if(compte == 0){
            print('llllll');
            widgets.insert(0,
                viewRender
            );
          } else {
            print('222');
            widgets.add(viewRender);

          }
          compte = 1;
        }
        else if(item['slug'] == 'coup-de-coeur'){
          widgets.add(
              Column(
                children: [
                  SizedBox(height: 30.0,),
                  InkWell(
                    onTap: () async{
                      var duration = await durationConvertion(item['articles'][0]['duree']);
                      Navigator.pushNamed(context, '/playerPodcast',
                          arguments: {
                            'audio': item['articles'][0]['audio'], //"https://luan.xyz/files/audio/nasa_on_a_mission.mp3",
                            'title': item['articles'][0]['title'],
                            'audioTitle': item['articles'][0]['audio_title'],
                            'cover' : item['articles'][0]['cover'],
                            'headerTitle' : item['name'].toUpperCase(),
                            'duration' : Duration(seconds: duration),
                            'type': 'coup-de-coeur'
                          });
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                              padding: EdgeInsets.only(left: 15.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(item['name'].toUpperCase(),
                                    style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 16.0),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis
                                  ),
                                  Text(item['articles'][0]['title'],
                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.0),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis
                                  ),
                                  SizedBox(height: 15.0,),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 15.0),
                                    child: Text("Maintenant disponible et diffusé sur Gu radio", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 12.0), ),
                                  ),
                                ],
                              )
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Image.network(item['articles'][0]['cover'])
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10.0,),
                ],
              )
          );

        }
        else if(item["slug"] == "bandeaux-pubs"){
            widgets.add(
              Column(
                children: [
                  SizedBox(height: 20.0,),
                  InkWell(
                    onTap: () async{
                      var url = item['link'];
                      if (await canLaunch(url)) {
                      await launch(url, forceSafariVC: false);
                      } else {
                      throw 'Could not launch $url';
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.all(15.0),
                        child: Image.network(item['articles'][0]['cover'])
                    ),
                  ),
                  SizedBox(height: 30.0,),
                ],
              )
          );
        }
      }
      return widgets;
  }

  durationConvertion(duration){
      var result;
      var data = duration.toString().split(':');
      if(data.length == 2){
        int minutes = int.parse(data[0]);
        int seconds = int.parse(data[1]);
        result = (minutes * 60) + seconds;
      } else if(data.length == 3){
          int hours = int.parse(data[0]);
          int minutes = int.parse(data[1]);
          int seconds = int.parse(data[2]);
          result = (hours * 3600) + (minutes * 60) + seconds;
      } else result = int.parse(data[0]);
     return result;
  }

  @override
  Widget build(BuildContext context) {
    guStore = Provider.of<GuStore>(context);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        backgroundColor: Colors.black,
        title: Center(child: Text('REPLAY DE LA RADIO', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)),
        leading: Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: IconButton(
            icon:  Icon(Icons.arrow_back_ios, color: Colors.white,),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 15.0),
          child: isLoading ? Center(child: CircularProgressIndicator()) :
          ListView(
            children: [
             ...widgetReplays(data)
            ],
          ),
        )
      ),
    );
  }
}
