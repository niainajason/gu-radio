import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gu_radio/helper/api.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:kenburns/kenburns.dart';
import 'package:native_webview/native_webview.dart';
import 'package:gu_radio/provider/GuStore.dart';
import 'package:provider/provider.dart';

class HomeRadio extends StatefulWidget {
  const HomeRadio({Key key}) : super(key: key);

  @override
  _HomeRadioState createState() => _HomeRadioState();
}

class _HomeRadioState extends State<HomeRadio> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  var data, dataContactBanners, dataContactLink, logoImg;
  bool isScrolled = false, isLoading = true, isLoadingContactScreen = true;
  String bgInDirectColor = '', bgReplayRadioColor = '', bgDirectTextColor = '', copyrightText = '';
  List<dynamic> imgList;
  GuStore guStore;
  ValueNotifier<bool> _notifier = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async{
      await getDataRadioScreen();
      await getDataContactScreen();
      await getLogoImg();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  getDataRadioScreen() async{
    data = await Network().fetchData("acf/v3/options/radio/");
    if(data['noConnection'] == null || data['noConnection'] == 'null') {
      setState(() {
        bgInDirectColor = data['acf']['direct-tag']['bg-color'].replaceAll('#', '0xff');
        bgReplayRadioColor = data['acf']['replay-btn']['bg-color'].replaceAll('#', '0xff');
        bgDirectTextColor = data['acf']['direct-tag']['direct-text-color'].replaceAll('#', '0xff');
        imgList = data['acf']['slides'];
        isLoading = false;
      });
    } else if(data['noConnection']) showSnackBar();
  }

  getDataContactScreen() async{
    var res = await Network().fetchData("acf/v3/options/contact/");
    if(res['noConnection'] == null || res['noConnection'] == 'null') {
      setState(() {
        isLoadingContactScreen = false;
        dataContactBanners = res['acf']['banners'];
        dataContactLink = res['acf'];
      });
    } else if(res['noConnection']) showSnackBar();
  }

  getLogoImg() async{
    var dataAsset = await Network().fetchData("acf/v3/options/assets/");
    print("getLogoImg");
    if(data['noConnection'] == null || data['noConnection'] == 'null') {
      setState(() {
        logoImg = dataAsset['acf']['logos']['logo-type-3'];
        copyrightText =  dataAsset['acf']['copyrights']['copyright'];
        isLoading = false;
      });
    } else if(data['noConnection']) showSnackBar();
  }

  showSnackBar(){
    final snackbar = SnackBar(content: Text('Verifier votre connexion', style: TextStyle(color: Colors.white)), backgroundColor: Colors.black);
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  isCarouselScrolled(){
    if(!_notifier.value){
      _notifier.value = !_notifier.value;
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    guStore = Provider.of<GuStore>(context);
    return Scaffold(
        key: scaffoldKey,
        body: SafeArea(
          child:
          isLoading ? Center(child: CircularProgressIndicator()) :
          Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
               child: Stack(
                  children : [
                    sliderWidget(),
                    Row (
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Ink(child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.grey,
                              onTap:(){
                                Navigator.pushNamed(context, "/contactUs");
                              },
                              child: Icon(Icons.more_vert, color: Colors.white, size: 30.0,)),
                        )
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              splashColor: Colors.grey.withOpacity(0.5),
                              onTap: () async{
                                const url = 'https://www.instagram.com/gospelurbain/?hl=en';
                                if (await canLaunch(url)) {
                                await launch(url, forceSafariVC: false);
                                } else {
                                throw 'Could not launch $url';
                                }
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height * 7 / 100,
                                width: MediaQuery.of(context).size.height * 7 / 100,
                                padding: EdgeInsets.symmetric(horizontal: 20.0),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    //color: Colors.grey,
                                    image: DecorationImage(
                                        image: AssetImage('assets/images/picto-reel-blanc.png'),
                                        fit: BoxFit.cover
                                    )
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Positioned(
                      top: kToolbarHeight-20,
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Align(
                        alignment: Alignment.topCenter,
                          child: InkWell(
                            onTap: (){
                              getDataRadioScreen();
                            },
                            child: Container(
                              height: MediaQuery.of(context).size.height * 15 / 100,
                              width: MediaQuery.of(context).size.height * 15 / 100,
                              child: logoImg != null ? Image.network(logoImg) : Image.asset("assets/images/icon_gu.png"),
                            ),
                          ),
                      ),
                    ),
                    if(!_notifier.value)
                      Positioned(
                        bottom: MediaQuery.of(context).size.height / 2,
                        right: 12.0,
                        child: ValueListenableBuilder(
                            valueListenable: _notifier,
                            builder: (BuildContext context, bool val, Widget child) {
                              if(!val){
                                  return  Column(
                                    children: [
                                      Icon(Icons.keyboard_arrow_up, color: Colors.white, size: 25.0,),
                                      Text('scroller', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                                      Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 25.0,),
                                    ],
                                  );
                              } else return Container();
                            }),
                    ),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        child: Column(
                          children: [
                            Stack(
                              children: [
                                Positioned(
                                    child: playerRadio()
                              ),
                                Positioned(
                                    top: 1.5,
                                    right: 0,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                                          color: Color(int.parse(bgInDirectColor)),
                                          child: Text(data['acf']['direct-tag']['title'], style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)),
                                    )
                                )
                              ]
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * 11 / 100,
                              color: Color(int.parse(data['acf']['replay-btn']['bg-color'].replaceAll('#', '0xff'))),
                              child: Ink(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                     splashColor: Colors.grey.withOpacity(0.3),
                                      onTap: (){
                                        Navigator.pushNamed(context, '/replayRadio');
                                      },
                                      child: Center(
                                        child: Text(
                                          data['acf']['replay-btn']['title'].toUpperCase(),
                                          style: TextStyle(
                                              fontSize: 25.0,
                                              color: Color(int.parse(data['acf']['replay-btn']['replay-text-color'].replaceAll('#', '0xff'))),
                                              letterSpacing: 2.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  )
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                ],
              ),
          ),
        ),
    );
  }
  Widget sliderWidget(){
    return Builder(
      builder: (context) {
        final double height = MediaQuery.of(context).size.height - ( MediaQuery.of(context).size.height * 23 / 100 + MediaQuery.of(context).size.height * 11 / 100);
        return CarouselSlider(
          options: CarouselOptions(
              height: height,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              scrollDirection: Axis.vertical,
              autoPlay: false,
              onPageChanged: (index, reason){
                 isCarouselScrolled();
              },
              reverse: true
          ),
          items: imgList.map((item)
          => Container(
              child: Stack(
                children: <Widget>[
                  Center(
                      child: InteractiveViewer(
                          panEnabled: false, // Set it to false
                          boundaryMargin: EdgeInsets.all(100),
                          minScale: 0.5,
                          maxScale: 22,
                          child: KenBurns(
                              maxAnimationDuration: Duration(seconds: 10),
                              minAnimationDuration: Duration(seconds: 3),
                              maxScale : 1.2,
                              child: Image.network(item['image'], fit: BoxFit.cover, height: height)
                          )
                      )
                  ),
                  Positioned(
                    bottom: height / 16,
                    left: 0.0,
                    right: 0.0,
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        child: Column(
                          children: [
                            Container(child: Image.network(item['logo'], fit: BoxFit.cover)),
                            Text(item['title'].toUpperCase(),
                                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
                            ),
                            Container(
                              color: item['sub-title'] != '' ? Colors.black : Colors.transparent,
                              padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 15.0),
                              child: Text(item['sub-title'].toUpperCase(),
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )
          )).toList(),
        );
      },
    );
  }
}

class playerRadio extends StatelessWidget {
  const playerRadio({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 19 / 100,
        child:  WebView(
          initialData: WebViewData("""
            <!doctype html>
            <html lang="en">
            <head>
              <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
              <style type="text/css">*{margin:0;padding: 0} iframe{border: 0;width: 100%}</style>
            </head>
            <body>
              <iframe src="https://www.radioking.com/widgets/player/player.php?id=371&c=%232F3542&c2=%23ffffff&f=h&i=0&ii=undefined&p=1&s=0&li=0&popup=0&plc=0&h=100&l=40&a=0&v=2&fullsize"></iframe>        
            </body>
            </html>
          """),
        )
    );
  }
}



