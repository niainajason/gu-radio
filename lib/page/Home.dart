
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gu_radio/widget/homeRadio/NotificationBadge.dart';
import 'package:gu_radio/helper/api.dart';
import 'package:video_player/video_player.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:gu_radio/model/PushNotification.dart';
import 'package:overlay_support/overlay_support.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  VideoPlayerController _controller;
  bool isLoading = true;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  var data;
  String logoImg;

  //Notification
  FirebaseMessaging _messaging;
  int _totalNotifications;
  PushNotification _notificationInfo;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async{
        await getHomeData();
        await getLogoImg();
    });

    //Notification
    _totalNotifications = 0;
    registerNotification();
    checkForInitialMessage();

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      PushNotification notification = PushNotification(
        title: message.notification?.title,
        body: message.notification?.body,
        dataTitle: message.data['title'],
        dataBody: message.data['body'],
      );
      setState(() {
        _notificationInfo = notification;
        _totalNotifications++;
      });
    });
    //EndNotification
  }

  // Notification
  void registerNotification() async {
    await Firebase.initializeApp();
    _messaging = FirebaseMessaging.instance;
    
    _messaging.getToken().then((token) {
      print('Token Device: $token');
    });

    await FirebaseMessaging.instance.subscribeToTopic('all');
    
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        // Parse the message received
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataTitle: message.data['title'],
          dataBody: message.data['body'],
        );

        setState(() {
          _notificationInfo = notification;
          _totalNotifications++;
        });

        if (_notificationInfo != null) {
          // For displaying the notification as an overlay
          showSimpleNotification(
            Text(_notificationInfo.title),
            leading: NotificationBadge(totalNotifications: _totalNotifications),
            subtitle: Text(_notificationInfo.body),
            background: Colors.grey.withOpacity(0.2),
            duration: Duration(seconds: 2),
          );
        }
      });
    }
  }

  checkForInitialMessage() async {
    await Firebase.initializeApp();
    RemoteMessage initialMessage =
    await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        dataTitle: initialMessage.data['title'],
        dataBody: initialMessage.data['body'],
      );

      setState(() {
        _notificationInfo = notification;
        _totalNotifications++;
      });
    }
  }

  getHomeData() async{
    data = await Network().fetchData("acf/v3/options/home");
    if(mounted){
      if(data['noConnection'] == null || data['noConnection'] == 'null') {
        if(data['acf']['home_data']['fond_video'] != false){
          _controller = VideoPlayerController.network(data['acf']['home_data']['fond_video']);
          _controller.setLooping(true);
          _controller.initialize().then((_) => setState(() {}));
          _controller.play();
        }
        setState(() {
          isLoading = false;
        });
      }
      else if(data['noConnection']) showSnackBar();
    }
  }

  getLogoImg() async{
    var dataAsset = await Network().fetchData("acf/v3/options/assets/");
    if(data['noConnection'] == null || data['noConnection'] == 'null') {
      setState(() {
        logoImg = dataAsset['acf']['logos']['logo-type-3'];
        isLoading = false;
      });
    } else if(data['noConnection']) showSnackBar();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  showSnackBar(){
    final snackbar = SnackBar(content: Text('Verifier votre connexion', style: TextStyle(color: Colors.white)), backgroundColor: Colors.black);
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.black,
      body: SafeArea(
        child: isLoading ? Center(child: CircularProgressIndicator()) : Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              SizedBox.expand(
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: data['acf']['home_data']['fond_image'] ?
                  Image.network(data['acf']['home_data']['fond_image'] ) :
                  SizedBox(
                     width: _controller.value.size?.width ?? 0,
                     height: _controller.value.size?.height ?? 0,
                    child: VideoPlayer(_controller),
                  ),
                ),
              ),
              Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 7.0),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.grey,
                              onTap: () {
                               Navigator.pushReplacementNamed(context, '/homeRadio');
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 5.0),
                                child: Text('PASSER L\'INTRO >>  ', style: TextStyle(
                                    color: Colors.white),),
                              )
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                      child: logoImg != null ? Image.network(logoImg) : Image.asset("assets/images/icon_gu.png")
                  ),
                  Expanded(flex: 2,child: Image.network(data['acf']['home_data']['infos'])),
                  Expanded(flex: 2,child: Image.network(data['acf']['home_data']['logo_restricted']))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
