import 'dart:async';
import 'dart:io' show Platform;

import 'package:audio_service/audio_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gu_radio/helper/AudioPlayerTask.dart';
import 'package:gu_radio/helper/SeekBar.dart';
import 'package:gu_radio/model/MediaState.dart';
import 'package:gu_radio/model/QueueState.dart';
import 'package:rxdart/rxdart.dart';

MediaItem mediaItemITest;

class PlayerPodcast extends StatefulWidget {
  @override
  _PlayerPodcastState createState() => _PlayerPodcastState();

}

class _PlayerPodcastState extends State<PlayerPodcast> {
  var dataArgument;
  MediaItem mediaItemITest;
  bool isStart = true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, (){
        AudioService.start(
          backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
          androidNotificationChannelName: 'Audio Service Demo',
          androidNotificationIcon: 'mipmap/ic_launcher',
          androidEnableQueue: true,
          params: {'url' : dataArgument['audio'], 'imageCover': dataArgument['cover'], 'title': dataArgument['title'] }
        );
    });
  }

  @override
  Widget build(BuildContext context) {
    dataArgument = ModalRoute.of(context).settings.arguments;
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Scaffold(
     backgroundColor: Colors.black,
      body: Center(
        child: StreamBuilder<bool>(
          stream: AudioService.runningStream,
          builder: (context, snapshot) {
            if (snapshot.connectionState != ConnectionState.active) {
              return SizedBox();
            }
            final running = snapshot.data ?? false;
            return SingleChildScrollView(
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: kToolbarHeight,),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        child: Ink(
                            child: InkWell(
                                onTap:(){
                                  Navigator.of(context).pop();
                                  AudioService.stop();
                                },
                                child: Icon(Icons.close, color: Colors.white, size: 25.0,))
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Text(dataArgument['headerTitle'],
                      style: TextStyle(
                          color: dataArgument['type'] == "coup-de-coeur" ? Colors.orange : Colors.yellow,
                          fontWeight: FontWeight.bold,
                          fontSize: 35.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  if (!running) ...[
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Image.network(dataArgument['cover']),
                        ),
                        SizedBox(height: kToolbarHeight / 2,),
                       ]
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(5.0)
                        ),
                        child: Row(
                          children: [
                            StreamBuilder<bool>(
                              stream: AudioService.playbackStateStream
                                  .map((state) => state.playing)
                                  .distinct(),
                              builder: (context, snapshot) {
                                final playing = snapshot.data ?? false;
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    if (playing) pauseButton() else playButton(),
                                  ],
                                );
                              },
                            ),
                            StreamBuilder<MediaState>(
                              stream: _mediaStateStream,
                              builder: (context, snapshot) {
                                final mediaState = snapshot.data;
                                return SeekBar(
                                  duration: dataArgument['duration'],
                                  position: mediaState?.position ?? Duration.zero,
                                  onChangeEnd: (newPosition) {
                                    AudioService.seekTo(newPosition);
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: kToolbarHeight / 3),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Text(dataArgument['title'].toUpperCase(), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),),
                          if(dataArgument['type'] == "coup-de-coeur")
                            Text(dataArgument['audioTitle'], style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),

                  ] else ...[
                    StreamBuilder<QueueState>(
                      stream: _queueStateStream,
                      builder: (context, snapshot) {
                        final queueState = snapshot.data;
                        final queue = queueState?.queue ?? [];
                        final mediaItem = queueState?.mediaItem;
                        return Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Image.network(dataArgument['cover']),
                            ),
                            SizedBox(height: kToolbarHeight / 6),
                          ],
                        );
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(5.0)
                        ),
                        child: Row(
                          children: [
                            // Play/pause/stop buttons.
                            StreamBuilder<bool>(
                              stream: AudioService.playbackStateStream
                                  .map((state) => state.playing)
                                  .distinct(),
                              builder: (context, snapshot) {
                                final playing = snapshot.data ?? false;
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    if (playing) pauseButton() else playButton(),
                                    // stopButton(),
                                  ],
                                );
                              },
                            ),
                            StreamBuilder<MediaState>(
                              stream: _mediaStateStream,
                              builder: (context, snapshot) {
                                final mediaState = snapshot.data;
                                return SeekBar(
                                  duration: dataArgument['duration'],
                                  //mediaState?.mediaItem?.duration ?? Duration.zero,
                                  position: mediaState?.position ?? Duration.zero,
                                  onChangeEnd: (newPosition) {
                                    AudioService.seekTo(newPosition);
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: kToolbarHeight / 6),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Text(dataArgument['title'].toUpperCase(), style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),),
                          if(dataArgument['type'] == "coup-de-coeur")
                            Text(dataArgument['audioTitle'], style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),
                  ],
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  /// A stream reporting the combined state of the current media item and its
  /// current position.
  Stream<MediaState> get _mediaStateStream =>
      Rx.combineLatest2<MediaItem, Duration, MediaState>(
          AudioService.currentMediaItemStream,
          AudioService.positionStream,
              (mediaItem, position) => MediaState(mediaItem, position));

  /// A stream reporting the combined state of the current queue and the current
  /// media item within that queue.
  Stream<QueueState> get _queueStateStream =>
      Rx.combineLatest2<List<MediaItem>, MediaItem, QueueState>(
          AudioService.queueStream,
          AudioService.currentMediaItemStream,
              (queue, mediaItem) => QueueState(queue, mediaItem));

  ElevatedButton startButton(String label, VoidCallback onPressed) =>
      ElevatedButton(
        child: Text(label),
        onPressed: onPressed,
      );

  IconButton playButton() => IconButton(
    icon: Icon(Icons.play_arrow),
    iconSize: 25.0,
    onPressed: (){
     if(!AudioService.running){
       AudioService.start(
           backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
           androidNotificationChannelName: 'Audio Service Demo',
           androidNotificationIcon: 'mipmap/ic_launcher',
           androidEnableQueue: true,
           params: {'url' : dataArgument['audio'], 'imageCover': dataArgument['cover'], 'title': dataArgument['title'] }
       );
     }
     AudioService.play();
    }
  );

  IconButton pauseButton() => IconButton(
    icon: Icon(Icons.pause),
    iconSize: 25.0,
    onPressed: AudioService.pause,
  );

  IconButton stopButton() => IconButton(
    icon: Icon(Icons.stop),
    iconSize: 25.0,
    onPressed: (){
      setState(() {
        isStart = false;
      });
      AudioService.stop();
    }
  );
}

// NOTE: Your entrypoint MUST be a top-level function.
void _audioPlayerTaskEntrypoint() async {
  AudioServiceBackground.run(() => AudioPlayerTask());
}

