import 'package:flutter/material.dart';
import 'package:gu_radio/page/Home.dart';
import 'package:gu_radio/page/HomeRadio.dart';
import 'package:gu_radio/page/PlayerPodcast.dart';
import 'package:gu_radio/page/ReplayRadio.dart';
import 'package:audio_service/audio_service.dart';
import 'package:gu_radio/provider/GuStore.dart';
import 'package:gu_radio/widget/homeRadio/contactUsWidget.dart';
import 'package:provider/provider.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:overlay_support/overlay_support.dart';


Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

void main() {
  runApp(
    OverlaySupport(
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => GuStore()),
        ],
        child: MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.grey,
          fontFamily: 'Roboto'
      ),
      // home: Home(),
      routes: {
         '/' : (context) => Home(),
        '/homeRadio' : (context) => HomeRadio(),
        '/replayRadio' : (context) => ReplayRadio(),
        '/playerPodcast' : (context) => AudioServiceWidget(child: PlayerPodcast()),
        '/contactUs' : (context) => ContactUs(),
      },
    );
  }
}

