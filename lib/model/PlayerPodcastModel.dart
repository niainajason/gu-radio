class PlayerPodcastModel{
  String url;
  String title;
  String artist;
  String duration;
  String coverImg;
  PlayerPodcastModel({this.url, this.title, this.artist, this.duration, this.coverImg});

  Map<String, dynamic> toMap (){
    var map = <String, dynamic>{
      'url' : url,
      'title': title,
      'artist' : artist,
      'duration' : duration,
      'coverImg' : coverImg,
    };
    return map;
  }

  PlayerPodcastModel.fromMap(Map<String, dynamic> map){
    url = map['url'];
    title = map['title'];
    artist = map['artist'];
    duration = map['duration'];
    coverImg = map['coverImg'];

  }
}