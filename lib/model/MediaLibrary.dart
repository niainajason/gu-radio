
import 'package:audio_service/audio_service.dart';

import 'package:rxdart/rxdart.dart';
class MediaLibrary {
  final _items = <MediaItem>[
    MediaItem(
      id: "https://s3.amazonaws.com/scifri-episodes/scifri20181123-episode.mp3",
      title: "Podcast",
    ),
  ];

  List<MediaItem> get items => _items;
}
