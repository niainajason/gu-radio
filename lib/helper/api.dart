import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:gu_radio/helper/config.dart';

class Network{
  String _baseUrl = urlAPI();
  Map<String, bool> checkConnection = {'noConnection' : true};

  Future fetchData(url) async {
    var fullUrl = _baseUrl + url;
    print('fulURL ' + fullUrl);
    String username = 'guradio';
    String password = 'Iz3hkVTdSaGykVCahx4olPUt';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));
    try{
      var r = await http.get(
        Uri.parse(fullUrl),
      ).timeout(const Duration(seconds: 5));
      if(r.statusCode == 200)
        return jsonDecode(r.body);
    } catch(e){
      return checkConnection;
    }
  }
}