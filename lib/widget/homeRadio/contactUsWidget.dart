import 'package:flutter/material.dart';
import 'package:gu_radio/helper/api.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:webview_flutter/webview_flutter.dart';


class ContactUs extends StatefulWidget {
  const ContactUs({Key key}) : super(key: key);

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {

  final scaffoldKey = GlobalKey<ScaffoldState>();
  var dataContactBanners, dataContactLink;
  bool  isLoadingContactScreen = true;
  String copyrightText = "";

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    Future.delayed(Duration.zero, () async{
      await getDataContactScreen();
    });
  }

  getDataContactScreen() async{
    var res = await Network().fetchData("acf/v3/options/contact/");
    if(res['noConnection'] == null || res['noConnection'] == 'null') {
      setState(() {
        isLoadingContactScreen = false;
        dataContactBanners = res['acf']['banners'];
        dataContactLink = res['acf'];
      });
    } else if(res['noConnection']) showSnackBar();
  }

  getLogoImg() async{
    var dataAsset = await Network().fetchData("acf/v3/options/assets/");
    print("getLogoImg");
    if(mounted){
      if(dataAsset['noConnection'] == null || dataAsset['noConnection'] == 'null') {
        copyrightText =  dataAsset['acf']['copyrights']['copyright'];
        isLoadingContactScreen = false;
      };
    } else if(dataAsset['noConnection']) showSnackBar();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: SafeArea(
        top: false,
        child: isLoadingContactScreen ? Center(child: CircularProgressIndicator()) : Container(
          height: MediaQuery.of(context).size.height ,
          color: Colors.black,
          child: Stack(
              children:[
                Positioned(
                  //top: kToolbarHeight,
                  child: Center(
                    child: ListView(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              child: Ink(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      splashColor: Colors.grey,
                                        onTap:(){
                                          Navigator.of(context).pop();
                                        },
                                        child: Icon(Icons.close, color: Colors.white, size: 25.0,)),
                                  )
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: kToolbarHeight / 10),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(dataContactLink['contact-us']['main-title'], style: TextStyle(color: Colors.white, fontSize: 30.0, fontWeight: FontWeight.bold),),
                            SizedBox(height: 10.0,),
                            Text(dataContactLink['contact-us']['email'], style: TextStyle(color: Colors.white, fontSize: 16.0),),
                            SizedBox(height: kToolbarHeight / 2),
                            Text(dataContactLink['contact-us']['follow-us']['titre'], style: TextStyle(color: Colors.white, fontSize: 30.0, fontWeight: FontWeight.bold),),
                            for(var item in dataContactLink['contact-us']['follow-us']['social-network'])
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                      splashColor: Colors.grey,
                                      onTap: () async {
                                        var url = item['link'];
                                        if (await canLaunch(url)) {
                                          await launch(url, forceSafariVC: false);
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                        child: Text(item['platform'], style: TextStyle(color: Colors.white, fontSize: 20.0),),
                                      )
                                  ),
                                ),
                              ),
                          ],
                        ),
                        SizedBox(height: 20.0,),
                        for(var item in dataContactBanners)
                          if(item['image'] != false)
                            Material(
                              color: Colors.transparent,
                              child: InkWell(
                                  splashColor: Colors.grey,
                                  onTap: () async{
                                    var url = item['link'];
                                    if (await canLaunch(url)) {
                                      await launch(url, forceSafariVC: false);
                                    } else {
                                      throw 'Could not launch $url';
                                    }
                                  },
                                  child: Image.network(item['image'])
                              ),
                            ),
                        Center(child: Text(copyrightText, style: TextStyle(color: Colors.white, fontSize: 14.0),)),
                      ],
                    ),
                  ),
                ),
              ]
          ),
        ),
      ),
    );
  }

  showSnackBar(){
    final snackbar = SnackBar(content: Text('Verifier votre connexion', style: TextStyle(color: Colors.white)), backgroundColor: Colors.black);
    scaffoldKey.currentState.showSnackBar(snackbar);
  }
}
