import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';

Widget radioWidget(context){
  return Container(
    width: MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height * 19 / 100,
    child: WebView(
      initialUrl: Uri.dataFromString(''
          '<html>'
          ' <head>'
          ' <meta name="viewport" '
          'content="width=device-width, initial-scale=1"> '
          '<style type="text/css">*{margin:0;padding: 0} iframe{border: 0;width: 100%}</style></head> '
          '<body> '
          '<iframe src="https://www.radioking.com/widgets/player/player.php?id=371&c=%232F3542&c2=%23ffffff&f=h&i=0&ii=undefined&p=1&s=0&li=0&popup=0&plc=0&h=100&l=40&a=0&v=2&fullsize"></iframe>'
          '</body>'
          '</html>', mimeType: 'text/html').toString(),
      gestureRecognizers: Set()..add(Factory<VerticalDragGestureRecognizer>(
               () => VerticalDragGestureRecognizer())
       ),
      javascriptMode: JavascriptMode.unrestricted,),
  );
}
