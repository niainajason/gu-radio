import 'package:flutter/material.dart';
import 'package:gu_radio/model/PlayerPodcastModel.dart';

class GuStore extends ChangeNotifier{

  PlayerPodcastModel _playerPodcastData = new PlayerPodcastModel();
  PlayerPodcastModel get playerPodcastData => _playerPodcastData;

  addPodcastToSession(id, title, artist, coverImg){
    print('Store');
    _playerPodcastData = new PlayerPodcastModel(url: id, title: title, artist: artist, coverImg: coverImg);
    print(_playerPodcastData);
    notifyListeners();
  }

  makeEmptySession(){
    print('Store for makeEmptySession');
    _playerPodcastData = null;
    notifyListeners();
  }

}